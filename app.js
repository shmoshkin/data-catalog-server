require('dotenv').config();
const logger = require('morgan');
const  bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');

const { verifyToken } = require('./src/utils/auth');
const {getUserByEmail} = require('./src/query/auth');
const {getSelectResult, isError} = require('./src/utils/graphql');
const app = express()

app.use(cors({credentials: true,origin: process.env.CORS}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger());
app.use(cookieParser());

app.get('/health', function (req, res) {
  res.sendStatus(200)
})

app.use('/auth', require('./src/routers/auth'))

app.use(async (req, res, next) => {
    
    const accessToken = req.cookies["access-token"];
    const refreshToken = req.cookies["refresh-token"];

    if(!refreshToken && !accessToken) return res.sendStatus(401);

    try{
        const {data: {email}} = verifyToken(accessToken)
        req.userId = email;
        return next();
    } catch{}
    
    if(!refreshToken) return res.sendStatus(401);

    try {
        const {data: {email}} = verifyToken(refreshToken, false)
        req.userId = email;
    } catch {
        return res.sendStatus(401)
    }

    const user = await to(axios({
        url: process.env.HASURA_ENDPOINT,
        method: 'post',
        headers: {
          'x-hasura-admin-secret': process.env.ADMIN_SECRET
        },
        data: {
          query: getUserByEmail(req.userId)
        }
      }))
    
    if (isError(user) || getSelectResult(user).block) {
        return res.sendStatus(401);
    }

    const tokens = await createTokens(user);

    res.cookie("refresh-token", tokens.refreshToken);
    res.cookie("access-token", tokens.accessToken);
    req.user = user;

    return next(req, res);
})


app.listen(process.env.PORT, () => console.log(`Data Catalog Server Kistening at http://localhost:${process.env.PORT}`))