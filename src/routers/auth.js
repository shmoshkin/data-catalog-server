const authRouter = require('express').Router();
const {signUp} = require('../bl/auth/signUp');
const {signIn} = require('../bl/auth/signIn');
const {refreshToken} = require('../utils/auth')

authRouter.post('/signIn/:email/:password', function (req, res) {
    const params = req.params;
    signIn(params.email, params.password, res)

})

authRouter.post('/signUp/:email/:password/:firstName/:lastName', function (req, res) {
    const params = req.params;
    signUp(params.email, params.password, params.firstName, params.lastName, res)
})

authRouter.get('/refreshToken', function (req, res) {
    
    refreshToken(req.cookies["access-token"], req.cookies["refresh-token"], res)
})

module.exports = authRouter