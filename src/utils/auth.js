const R = require('ramda');
const jwt = require('jsonwebtoken');
const to = require('await-to-js').default;
const axios = require('axios');
const {getUserByEmail} = require('../query/auth');
const {getSelectResult, isError} = require('../utils/graphql');

const getTokenPayload = ({firstName, lastName, email, admin, role}) => ({
  "name": `${firstName} ${lastName}`,
  "email": email,
  "admin": admin,
  "role": admin ? "admin" : role,
  "https://hasura.io/jwt/claims": {
    "x-hasura-allowed-roles": ["admin","user","anonymous"],
    "x-hasura-default-role": "anonymous",
    "x-hasura-role": admin ? "admin" : role,
    "x-hasura-user-id": email,
    "x-hasura-custom": "group",
  }
})

const getToken = async (user, isAccessToken = true)  => {

    const token = await jwt.sign(
      getTokenPayload(user),    
      isAccessToken? process.env.SECRET : process.env.REFRESH_SECRET,
      { 
        algorithm: 'HS256',
        expiresIn: isAccessToken ? '365d' : '365d'
      }
    )
    
  return token;
}

const getTokens = async (user) => {
  const accessToken = await getToken(user)
  const refreshToken = await getToken(user, false)

  return {accessToken, refreshToken}
}

const verifyToken = (token, isAccessToken = true) => jwt.verify(token,isAccessToken ? process.env.SECRET : process.env.REFRESH_SECRET)

const refreshToken = async (accessToken, refreshToken, res) => {
    let verifyRes = null;
    if(!refreshToken && !accessToken) return res.sendStatus(401);

    try{
        verifyRes = verifyToken(accessToken)
        return res.sendStatus(200);
    } catch{}
    
    if(!refreshToken) return res.sendStatus(401);

    try {
      verifyRes = verifyToken(refreshToken, false)
    } catch (e){
        return res.sendStatus(401)
    }

    const [err,userRes] = await to(axios({
        url: process.env.HASURA_ENDPOINT,
        method: 'post',
        headers: {
          'x-hasura-admin-secret': process.env.ADMIN_SECRET
        },
        data: {
          query: getUserByEmail(verifyRes.email)
        }
      }))
    
    if (err || isError(userRes)) {
        return res.sendStatus(401);
    }


    const user = getSelectResult(userRes, "users")[0]
    
    if (user.block) return res.sendStatus(401)
    const tokens = await getTokens(user);

    res.cookie("refresh-token", tokens.refreshToken);
    res.cookie("access-token", tokens.accessToken);    

    res.sendStatus(200);
}

module.exports = {
  getToken,
  verifyToken,
  getTokens,
  refreshToken
}