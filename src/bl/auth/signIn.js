const bcrypt = require('bcrypt');
const axios = require("axios")
const to = require('await-to-js').default;

const {getUserByEmail} = require('../../query/auth')
const {isError, getSelectResult} = require('../../utils/graphql')
const {getTokens} = require('../../utils/auth')

const signIn = async (email, password, res) => {
  
  const [err,userRes] = await to(axios({
    url: process.env.HASURA_ENDPOINT,
    method: 'post',
    headers: {
      'x-hasura-admin-secret': process.env.ADMIN_SECRET
    },
    data: {
      query: getUserByEmail(email)
    }
  }))
  
  if (err || isError(userRes)){
    return res.status(401).json({message: "User not found"})
  } else {  
    const user = getSelectResult(userRes, "users")[0]
    
    if(user.block) return res.status(401).json({message: "User is blocked"})

    const match = await bcrypt.compare(password, user.password);

    if(!match) {
      return res.status(401).json({message: "Password incorrect"})
    } else {
      
      const tokens = await getTokens(user);
      res.cookie("refresh-token", tokens.refreshToken)
      res.cookie("access-token", tokens.accessToken)

      return res.status(200).json({
        message: "Sign in successfully"
      })
    }
  }
}

module.exports = {
    signIn
}